INTRODUCTION
------------

The Commerce Discount Time module allows the select a time while setting up
a date range for your discount.

 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/gurksor/2979092

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2979092


REQUIREMENTS
------------

This module requires the following modules:

 * Commerce Discount (https://drupal.org/project/commerce_discount)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow you to select a time on the discount configuration page.
To get the default behaivor back, disable the module and clear caches.


MAINTAINERS
-----------

Current maintainers:
 * Alexander Gura (gurksor) - https://drupal.org/user/3269280

This project has been sponsored by:
 * Fewclicks GmbH
   Visit https://fewclicks.io for more information.
