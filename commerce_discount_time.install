<?php
/**
 * @file
 * Install/Uninstall hooks for Commerce Discount Time
 */

/**
 * Implements hook_enable().
 */
 function commerce_discount_time_enable() {
  // Alter commerce discount date field
  if (($commerce_discount_date = field_info_field('commerce_discount_date'))) {
    $commerce_discount_date['settings']['granularity'] = array(
      'month' => 'month',
      'day' => 'day',
      'year' => 'year',
      'hour' => 'hour',
      'minute' => 'minute',
    );

    field_update_field($commerce_discount_date);
  }
}

 /**
  * Implements hook_disable().
  *
  * Resets the granularity back to default and convert the date conditions back to normal
  */
function commerce_discount_time_disable() {
  // Remove added granularity
  if (($commerce_discount_date = field_info_field('commerce_discount_date'))) {
    $commerce_discount_date['settings']['granularity'] = array(
      'month' => 'month',
      'day' => 'day',
      'year' => 'year',
    );

    field_update_field($commerce_discount_date);
  }

  foreach (commerce_discount_types() as $type => $value) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_discount')
      ->entityCondition('bundle', $type)
      ->fieldCondition('commerce_discount_date', 'value', 'NULL', '!=');

    $result = $query->execute();

    if (isset($result['commerce_discount'])) {
      $discounts = entity_load('commerce_discount', array_keys($result['commerce_discount']));

      foreach($discounts as $discount) {
        $discount->commerce_discount_date[LANGUAGE_NONE][0]['value'] = strtotime(date(
          'm/d/Y 00:00:00',  
          $discount->commerce_discount_date[LANGUAGE_NONE][0]['value']
        ));

        if (!empty($discount->commerce_discount_date[LANGUAGE_NONE][0]['value2'])) {
          $discount->commerce_discount_date[LANGUAGE_NONE][0]['value2'] = strtotime(date(
            'm/d/Y 00:00:00',  
            $discount->commerce_discount_date[LANGUAGE_NONE][0]['value2']
          ));
        }

        entity_save('commerce_discount', $discount);
      }
    }
  }

  drupal_set_message(
    t('Hours and Minutes have been removed from your discounts. The existing dates were automatically adjusted to the default discount date behavior (m/d/Y 00:00)'), 
    'warning'
  );
}